<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Content;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('version')
            ->add('description')
            ->add('summary')
            ->add('iconFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => true,
                'download_label' => static function (Content $content) {
                    return $content->getName();
                },
                'download_uri' => true,
                'image_uri' => true,
                'asset_helper' => true,
            ])
            ->add('type', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
            ])
            ->add('artifacts', CollectionType::class, [
                'entry_type' => ArtifactType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('submit', SubmitType::class, ['label' => 'Publish']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Content::class,
        ]);
    }
}
