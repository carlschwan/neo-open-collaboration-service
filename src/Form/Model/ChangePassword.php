<?php

namespace App\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChangePassword
 * @package App\Form\Model
 */
class ChangePassword
{
    /**
     * @SecurityAssert\UserPassword(
     *     message = "L'ancien mot de passe est faux"
     * )
     */
    private string $oldPassword = "";

    /**
     * @Assert\Length(
     *      min = 8,
     *      max = 200,
     *      minMessage = "Le mot de passe doit être plus long que {{ limit }} charactêres.",
     *      maxMessage = "Le mot de passe ne doit pas être plus long que {{ limit }} charactêres."
     * )
     */
    private string $newPassword = "";

    /**
     * @return string
     */
    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    /**
     * @return string
     */
    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     */
    public function setNewPassword(string $newPassword): void
    {
        $this->newPassword = $newPassword;
    }

    /**
     * @param string $oldPassword
     */
    public function setOldPassword(string $oldPassword): void
    {
        $this->oldPassword = $oldPassword;
    }
}
