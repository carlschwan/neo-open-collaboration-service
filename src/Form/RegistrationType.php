<?php
// SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace App\Form;

use App\Entity\User;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['edit']) {
            $builder
                ->add('plainPassword', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'first_options' => ['label' => 'Password'],
                    'second_options' => ['label' => 'Password (repeat)'],
                    'invalid_message' => 'The two passwords are not identical',
                ])
                ->add('termOfUse', CheckboxType::class, [
                    'label' => 'I read the term of use.',
                ])
                ->add('submit', SubmitType::class, [
                   'label' => 'Create an account',
                    'attr' => ['class' => 'btn btn-success'],
                ]);
        } else {
            $builder->add('submit', SubmitType::class, [
                'label' => 'Edit my account',
                'attr' => ['class' => 'btn btn-success'],
            ]);
        }
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email Address',
            ])
            ->add('username', TextType::class, [
                'label' => 'Username',
            ])
            ->add('fullname', TextType::class, [
                'label' => 'Fullname',
            ])
            ->add('homepage', TextType::class, [
                'label' => 'Homepage',
                'required' => false,
            ]);
        if (!$options['edit']) {
            $builder->add('captcha', CaptchaType::class, [
                'label' => 'Please complete the captcha.'
            ]);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'edit' => false,
        ]);
        $resolver->setAllowedTypes('edit', 'bool');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }


}
