<?php

namespace App\Controller;

use App\Entity\Content;
use App\Form\ContentType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ContentController extends AbstractController
{

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/content/add", name="app_content_add")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $em): Response
    {
        $content = new Content();
        $content->setAuthor($this->getUser());
        $form = $this->createForm(ContentType::class, $content);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($content->getArtifacts() as $artifact) {
                $artifact->setContent($content);
                $em->persist($artifact);
            }
            $em->persist($content);
            $em->flush();
            return $this->redirectToRoute('app_profile');
        }

        return $this->render('content/add.html.twig', [
            'title' => "Add new content",
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/content/{id}", name="app_content_view")
     * @param Content $content
     * @return Response
     */
    public function view(Content $content): Response
    {
        return $this->render('content/view.html.twig', [
            'content' => $content,
        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/content/edit/{id}", name="app_content_edit")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function edit(Content $content, Request $request, EntityManagerInterface $em): Response
    {
        if ($content->getAuthor() != $this->getUser()) {
            throw new UnauthorizedHttpException("You can't edit the content from someone else.");
        }
        $form = $this->createForm(ContentType::class, $content);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($content);
            $em->flush();
            return $this->redirectToRoute('app_profile');
        }

        return $this->render('content/add.html.twig', [
            'title' => "Add new content",
            'form' => $form->createView(),
        ]);
    }
}
