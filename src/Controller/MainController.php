<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Content;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage", defaults={"_locale", "en"})
     * @return Response
     */
    public function index(EntityManagerInterface $em): Response
    {
        /** @var NestedTreeRepository $repository */
        $repository = $em->getRepository(Category::class);

        $categories = $repository->childrenHierarchy(null, true);

        $contentRepository = $em->getRepository(Content::class);
        $featuredContent = $contentRepository->findAll();
        return $this->render('main/index.html.twig', [
            'featuredContent' => $featuredContent,
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/category/{id}", name="app_category")
     * @param Category $category
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function category(Category $category, EntityManagerInterface $em): Response
    {
        /** @var NestedTreeRepository $repository */
        $repository = $em->getRepository(Category::class);
        $categories = $repository->getChildren($category);

        return $this->render('main/category.html.twig', [
            "categories" => $categories,
            "category" => $category,
        ]);
    }
}
