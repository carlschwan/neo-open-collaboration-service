<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\Model\ChangePassword;
use App\Form\Model\ResetPassword;
use App\Form\Model\ResetPasswordRequest;
use App\Form\RegistrationType;
use App\Form\ResetPasswordRequestType;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_homepage');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/profile", name="app_profile")
     * @return Response
     */
    public function viewProfile(): Response
    {
        $user = $this->getUser();
        $changePassword = new ChangePassword();

        $form = $this->createForm(ChangePasswordType::class, $changePassword);
        return $this->render('profile/show.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/profile/passwd", name="app_security_passwd")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param FlashBagInterface $flashBag
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder,
                                   FlashBagInterface $flashBag, EntityManagerInterface $em): Response
    {
        $changePassword = new ChangePassword();

        $form = $this->createForm(ChangePasswordType::class, $changePassword);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $user->setPassword($passwordEncoder->encodePassword($user, $changePassword->getNewPassword()));
            $em->persist($user);
            $em->flush();

            $flashBag->add('success', 'Password edited');
        }

        return $this->redirect($this->generateUrl('app_profile'));
    }

    /**
     * @Route("/profile/new", name="app_security_registration")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $em
     * @param \Swift_Mailer $mailer
     * @param FlashBagInterface $flashBag
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em, \Swift_Mailer $mailer, FlashBagInterface $flashBag,
        UrlGeneratorInterface $router
    )
    {
        if ($this->getUser() !== null) {
            $flashBag->add('error', 'You already have an account');
            return $this->redirectToRoute('profile');
        }
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setEnabled(false);

            $user->setConfirmationToken(md5(random_bytes(60)));
            $em->persist($user);
            $em->flush();

            $confirmationUrl = $router->generate('app_profile_validate', [
                'confirmationToken' => $user->getConfirmationToken()
            ], true);

            $email = (new \Swift_Message('Conplete your registration'))
                ->setFrom(['noreply@kde.org' => 'KDE Look'])
                ->setTo($user->getEmail())
                ->setBody("Hello!

To confirm you KDE look accout, please go to {$confirmationUrl}.

This link can be used only once.

Thanks,
The KDE look team");

            $mailer->send($email);

            return $this->redirectToRoute('app_profile_registration_info', ['email' => $user->getEmail()]);
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profile/registration/info/", name="app_profile_registration_info")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function registrationInfo(Request $request): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('profile');
        } else {
            return $this->render('security/register_info.html.twig', [
                'email' => $request->query->get('email'),
            ]);
        }
    }

    /**
     * @Route("/profile/validate/{confirmationToken}", name="app_profile_validate")
     * @param string $confirmationToken
     * @param EntityManagerInterface $em
     * @param FlashBagInterface $flashBag
     * @return RedirectResponse|Response
     */
    public function validateEmail(string $confirmationToken, EntityManagerInterface $em,
                                  FlashBagInterface $flashBag): Response
    {
        $userRepository = $em->getRepository(User::class);

        /** @var User|null $user */
        $user = $userRepository->findOneBy(['confirmationToken' => $confirmationToken]);
        if ($user) {
            $user->setEnabled(true);
            $user->setConfirmationToken(null);
            $em->persist($user);
            $em->flush();
            $flashBag->add('success',
                'Your account has been validated. You can now login.');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/wrong-token.html.twig');
    }

    /**
     * @Route("/profile/edit", name="app_profile_edit")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     */
    public function editAccount(Request $request, EntityManagerInterface $em)
    {

        $user = $this->getUser();
        $form = $this->createForm(RegistrationType::class, $user, ['edit' => true]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('profile');
        }

        return $this->render('profile/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/reset-password", name="app_security_reset_password")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param \Swift_Mailer $mailer
     * @return Response
     * @throws \Exception
     */
    public function resetPassword(Request $request, EntityManagerInterface $em, \Swift_Mailer $mailer,
                                  UrlGeneratorInterface $router): Response
    {
        $resetPassword = new ResetPasswordRequest();
        $form = $this->createForm(ResetPasswordRequestType::class, $resetPassword);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UserRepository $userRepository */
            $userRepository = $em->getRepository(User::class);
            $user = $userRepository->findOneBy(['email' => $resetPassword->getEmail()]);

            if ($user) {
                $user->setConfirmationToken(md5(random_bytes(60)));
                $em->persist($user);
                $em->flush();

                $confirmationUrl = $router->generate('app_security_reset_password_token', [
                    'confirmationToken' => $user->getConfirmationToken()
                ], true);

                $email = (new \Swift_Message('Initialize your password'))
                    ->setTo($user->getEmail())
                    ->setFrom(['noreply@kde.org' => 'KDE Look'])
                    ->setBody('Click on this link to initialize your password:' . $confirmationUrl);

                $mailer->send($email);
            }

            return $this->render('security/reset-password-send.html.twig');
        }
        return $this->render('security/reset-password-form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/reset-password/{token}", name="app_security_reset_password_token")
     * @param string $token token from url
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param FlashBagInterface $flashBag
     * @return Response
     */
    public function resetPasswordValidation(string $token, Request $request, EntityManagerInterface $em,
                                            UserPasswordEncoderInterface $passwordEncoder,
                                            FlashBagInterface $flashBag): Response
    {
        $resetPassword = new ResetPassword();
        $form = $this->createForm(ResetPasswordType::class, $resetPassword);

        $form->handleRequest($request);

        /** @var UserRepository $userRepository */
        $userRepository = $em->getRepository(User::class);
        $user = $userRepository->findOneBy(['confirmationToken' => $token]);

        if ($user === null) {
            $flashBag->add('error',
                'Wrong link. Please try again or contact us: somemail@kde.org');
            return $this->redirectToRoute('app_security_reset_password');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($passwordEncoder->encodePassword($user, $resetPassword->getPassword()));
            $user->setConfirmationToken(null);
            $em->persist($user);
            $em->flush();
            $flashBag->add('success', 'Password initialized');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/reset-password-form-pass.html.twig', [
            'token' => $token,
            'form' => $form->createView(),
        ]);
    }
}
