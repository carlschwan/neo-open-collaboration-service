<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Content;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            // the name visible to end users
            ->setTitle('Admin interface')

            // the path defined in this method is passed to the Twig asset() function
            ->setFaviconPath('favicon.svg')
            ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        // yield MenuItem::linkToCrud('The Label', 'icon class', EntityClass::class);

        yield MenuItem::section('User');
        yield MenuItem::linkToCrud('Users', 'fa fa-tags', User::class);

        yield MenuItem::section('Content');
        yield MenuItem::linkToCrud('Category', 'fa fa-tags', Category::class);
        yield MenuItem::linkToCrud('Content', 'fa fa-tags', Content::class);
    }
}
