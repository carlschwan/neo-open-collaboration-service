<?php

namespace App\Controller\ApiV1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/api/v1/provider.xml", name="api_v1_provider", defaults={"_format": "xml"})
     */
    public function index()
    {
        return $this->render('api_v1/main/index.xml.twig');
    }
}
