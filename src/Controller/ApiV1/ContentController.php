<?php

namespace App\Controller\ApiV1;

use App\Entity\Artifact;
use App\Entity\Category;
use App\Entity\Content;
use App\Repository\ContentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Exception\Config\Filter\NotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContentController
 * @package App\Controller\ApiV1
 * @route("/api/v1")
 */
class ContentController extends AbstractController
{
    /**
     * @Route("/content/categories.{_format}", defaults={"_format" = "xml"})
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function categories(EntityManagerInterface $em): Response
    {
        $categories = $em->getRepository(Category::class)->findAll();
        return $this->render('api_v1/content/categories.xml.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/content/data/{id}", name="app_api_v1_content_get", defaults={"_format" = "xml"})
     * @param Content $content
     * @return Response
     */
    public function getContent(Content $content, EntityManagerInterface $em): Response
    {
        return $this->render('api_v1/content/get.xml.twig', [
            'content' => $content,
        ]);
    }

    /**
     * @Route("/content/download/{id}/{artifact_id}", name="app_api_v1_content_get", defaults={"_format" = "xml"})
     * @ParamConverter("content", options={"id" = "id"})
     * @param Content $content
     * @return Response
     */
    public function downloadContent(Content $content, int $artifact_id, EntityManagerInterface $em): Response
    {
        $artifact = $content->getArtifacts()->get($artifact_id - 1);
        if ($artifact === NULL) {
            throw new NotFoundException();
        }
        return $this->render('api_v1/content/download.xml.twig', [
            'artifact' => $artifact,
        ]);
    }

    /**
     * @Route("/content/data.{_format}", defaults={"_format" = "xml"})
     * @param EntityManagerInterface $em
     * @param Request $request
     * @return Response
     */
    public function list(EntityManagerInterface $em, Request $request): Response
    {
        /** @var ContentRepository $contentRepository */
        $contentRepository = $em->getRepository(Content::class);
        $contentCount = $contentRepository->count([]);

        $page = $request->get('page') ?? 0;
        $pageSize = $request->get('pagesize') ?? 10;
        $pageSize = max(1, min(100, $pageSize));
        $contents = NULL;
        if ($categories = $request->get('categories')) {
            $contents = $contentRepository->findByCategories($categories, $pageSize, $page * $pageSize);
        } else if ($search = $request->get('search')) {
            // TODO implement search in repository
            $contents = $contentRepository->findBy(['name' => $search], [], $pageSize, $page * $pageSize);
        } else {
            $contents = $contentRepository->findBy([], [], $pageSize, $page * $pageSize);
        }
        return $this->render('api_v1/content/list.xml.twig', [
            'contents' => $contents,
            'contentCount' => $contentCount,
            'itemsPerPage' => $pageSize,
        ]);
    }
}
