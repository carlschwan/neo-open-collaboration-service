<?php

namespace App\Entity;

use App\Repository\ContentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ContentRepository::class)
 * @Vich\Uploadable
 */
class Content
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="contents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="contents")
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorAlt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $summary;

    /**
     * @Vich\UploadableField(mapping="content_icon", fileNameProperty="iconName")
     *
     * @var File|null
     */
    private $iconFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $iconName;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface|null
     */
    private $updatedIconAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $iconExternal;

    /**
     * @ORM\Column(type="array")
     */
    private $previewNames = [];

    /**
     * @ORM\OneToMany(targetEntity=Artifact::class, mappedBy="content", orphanRemoval=true)
     */
    private $artifacts;

    /**
     * @ORM\Column(type="boolean")
     */
    private $featured;

    /**
     * @ORM\Column(type="boolean")
     */
    private $approved;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        $this->updatedIconAt = new \DateTime();
        $this->iconExternal = false;
        $this->artifacts = new ArrayCollection();
        $this->approved = false;
        $this->featured = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function getAuthorName(): ?string
    {
        if ($this->authorAlt != null) {
            return $this->authorAlt;
        }
        return $this->author->getUsername();
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getType(): ?Category
    {
        return $this->type;
    }

    public function setType(?Category $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getAuthorAlt(): ?string
    {
        return $this->authorAlt;
    }

    public function setAuthorAlt(?string $authorAlt): self
    {
        $this->authorAlt = $authorAlt;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getIconName(): ?string
    {
        return $this->iconName;
    }

    public function setIconName(?string $iconName): self
    {
        $this->iconName = $iconName;

        return $this;
    }

    public function getIconExternal(): ?bool
    {
        return $this->iconExternal;
    }

    public function setIconExternal(bool $iconExternal): self
    {
        $this->iconExternal = $iconExternal;

        return $this;
    }

    public function getPreviewNames(): ?array
    {
        return $this->previewNames;
    }

    public function setPreviewNames(array $previewNames): self
    {
        $this->previewNames = $previewNames;

        return $this;
    }

    /**
     * @param File|UploadedFile|null $iconFile
     * @return Content
     */
    public function setIconFile($iconFile = null): self
    {
        $this->iconFile = $iconFile;

        if (null !== $iconFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedIconAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getIconFile()
    {
        return $this->iconFile;
    }

    /**
     * @return Collection|Artifact[]
     */
    public function getArtifacts(): Collection
    {
        return $this->artifacts;
    }

    public function addArtifact(Artifact $artifact): self
    {
        if (!$this->artifacts->contains($artifact)) {
            $this->artifacts[] = $artifact;
            $artifact->setContent($this);
        }

        return $this;
    }

    public function removeArtifact(Artifact $artifact): self
    {
        if ($this->artifacts->contains($artifact)) {
            $this->artifacts->removeElement($artifact);
            // set the owning side to null (unless already changed)
            if ($artifact->getContent() === $this) {
                $artifact->setContent(null);
            }
        }

        return $this;
    }

    public function getFeatured(): ?bool
    {
        return $this->featured;
    }

    public function setFeatured(bool $featured): self
    {
        $this->featured = $featured;

        return $this;
    }

    public function getApproved(): ?bool
    {
        return $this->approved;
    }

    public function setApproved(bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }
}
