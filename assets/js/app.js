/**
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-LicenseRef: AGPL-3.0-or-later
 */

import '../css/app.scss';
import 'jquery';
import 'bootstrap';
