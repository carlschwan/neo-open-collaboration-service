<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200724005644 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE artifact ADD content_id INT NOT NULL, ADD external TINYINT(1) NOT NULL, ADD file_name VARCHAR(255) NOT NULL, ADD file_size INT NOT NULL, ADD updated_at DATETIME NOT NULL, ADD created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE artifact ADD CONSTRAINT FK_48E5602C84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('CREATE INDEX IDX_48E5602C84A0A3ED ON artifact (content_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE artifact DROP FOREIGN KEY FK_48E5602C84A0A3ED');
        $this->addSql('DROP INDEX IDX_48E5602C84A0A3ED ON artifact');
        $this->addSql('ALTER TABLE artifact DROP content_id, DROP external, DROP file_name, DROP file_size, DROP updated_at, DROP created_at');
    }
}
