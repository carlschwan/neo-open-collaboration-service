## Development environment

```
# Create database
sudo mysql
CREATE USER 'neoocs'@'localhost' IDENTIFIED BY '<secret>';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, INDEX, DROP, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON neoocs.* TO 'neoocs'@'localhost';
FLUSH PRIVILEGES;

# Install dependencies
composer install

# Configure server
cp .env .env.local
vim .env.local # edit database configuration
bin/console doctrine:database:create
bin/console doctrine:migrations:migrate

# Start server
cd public
php -S localhost:8000

# open https://localhost:8000/en
```
